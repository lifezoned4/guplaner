export const getCardInfosFromCache = async () => {
  const lastCardInfos = JSON.parse(localStorage.getItem("cardInfos"));
  const fetchTime = Math.floor(Date.now() / 1000)
  if (lastCardInfos && lastCardInfos["fetchTime"] + 300 > fetchTime) {
    console.log("Used Cache");
    return lastCardInfos;
  }
  const totalItems = fetch("https://api.godsunchained.com/v0/proto?perPage=100&page=1").then((response) => response.json())
    .then((data) => {
      console.log(data);
      if (data) return data["total"]
    });

  const getCardInfos = (currentPage, resolve) => {
    fetch(`https://api.godsunchained.com/v0/proto?perPage=100&page=${currentPage}`).then((response) => response.json()).then((result): [] => resolve(result["records"]));
  }

  console.log(await totalItems)

  const pageCount = Math.ceil(await totalItems / 100);

  console.log(pageCount);
  const pageNumbers = [...Array(pageCount).keys()].map(i => i + 1);
  const allResults = (await Promise.all(pageNumbers.map((i) => new Promise<[]>((resolve) => setTimeout(() => getCardInfos(i, resolve), i * 1000)))))
  console.log(allResults);
  const cacheResult = allResults.flat();

  const cardInfos = {
    "fetchTime": fetchTime,
    "infos": cacheResult
  }
  localStorage.setItem("cardInfos", JSON.stringify(cardInfos))
  return cardInfos;
}
