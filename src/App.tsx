import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";

import type { DropEvent } from "@mirohq/websdk-types";

import { getCardInfosFromCache } from "./sharedCode";

import * as guDeckcode from "@imtbl/gu-deckcode";

const { board } = miro;

function App() {

  const [internalInputValue, setInternalInputValue] = useState("");

  const images = [

  ];

  const drop = async (e: DropEvent) => {
    const { x, y, target } = e;

    if (target instanceof HTMLImageElement) {
      const image = await board.createImage({ x, y, url: target.src });
      await board.viewport.zoomTo(image);
    }
  };

  // Register the drop event handler once.
  useEffect(() => {
    board.ui.on("drop", drop);
  });

  const placeCard = async () => {
    console.log(internalInputValue);

    const foundProtoIds = (await getCardInfosFromCache())["infos"].filter((value) => value.name.includes(internalInputValue));
    console.log(foundProtoIds);

    if (foundProtoIds.length == 1) {
      const viewport = await miro.board.viewport.get();
      const image = await miro.board.createImage({
        title: foundProtoIds[0].name,
        url: `https://card.godsunchained.com/?id=${foundProtoIds[0].id}&q=4&png=true`,
        x: viewport.x + viewport.width / 2 + 100 / 2, // Default value: horizontal center of the board
        y: viewport.y + viewport.height / 2, // Default value: vertical center of the board
        width: 100, // Set either 'width', or 'height'
        rotation: 0.0,
      });
      image.setMetadata("cardData", foundProtoIds[0]);
    }
  };

  const encodeDeck = async () => {
    const selection = miro.board.getSelection();
    const cards = (await selection).length;
    console.log(`Try to encode deck with: ${cards}`);
    const cardsToEncode = [];
    let domain = "";
    for(const card of (await selection)) {
      const cardInfo =  await card.getMetadata("cardData")
      cardsToEncode.push(cardInfo["lib_id"]);
      if(cardInfo["god"] != "neutral") {
        domain = cardInfo["god"];
      }
    }


    console.log(cardsToEncode);
    console.log(guDeckcode.encode(cardsToEncode, domain));
    
  }

  const addDeck = async () => {
    const { libraryIds, domain, formatCode } = guDeckcode.decode(internalInputValue);

    const cardInfos = (await getCardInfosFromCache())["infos"];

    const resultingCards = libraryIds.map(libId => cardInfos.find((info) => info.lib_id == libId));

    const manaBuckets = {};

    for (const index in resultingCards) {
      if (!manaBuckets[resultingCards[index].mana]) {
        manaBuckets[resultingCards[index].mana] = []
      }
      manaBuckets[resultingCards[index].mana].push(resultingCards[index])
    }

    const viewport = await miro.board.viewport.get();

    let count = 0;
    for (const mana in manaBuckets) {
      let index_count = 0;
      let lastCard;
      for (const index in manaBuckets[mana]) {
        let shift = false;
        if (lastCard && lastCard.id == manaBuckets[mana][index].id) {
          index_count -= 1;
          shift = true;
        }
        const image = await miro.board.createImage({
          title: manaBuckets[mana][index].name,
          url: `https://card.godsunchained.com/?id=${manaBuckets[mana][index].id}&q=4&png=true`,
          x: viewport.x + viewport.width / 2 + 100 / 2 + parseInt(index_count) * 120 + (shift ? 10 : 0), // Default value: horizontal center of the board
          y: viewport.y + viewport.height / 2 + count * 150 + (shift ? 10 : 0), // Default value: vertical center of the board
          width: 100, // Set either 'width', or 'height'
          rotation: 0.0,
        });

        image.setMetadata("cardData", manaBuckets[mana][index]);

        index_count += 1;
        lastCard = manaBuckets[mana][index];
      }
      count += 1;
    }
  }

  return (
    <div className="main">
      <div>
        <label for="example-1">Input: </label>
        <input class="input" type="text" placeholder="Card name or deck string"
          onChange={(e) => {
            setInternalInputValue(e.target.value);
          }}
          id="exampleField" />
      </div>
      {images.map((image, index) => {
        return (
          <img
            src={image}
            draggable={false}
            className="miro-draggable draggable-item"
            key={index}
          />
        );
      })}
      <button class="button button-primary" type="button" onClick={placeCard}>
        Add Card
      </button>
      <button class="button button-primary" type="button" onClick={addDeck}>
        Decode Deck
      </button>
      <button class="button button-primary" type="button" onClick={encodeDeck}>
        Encode Deck
      </button>
    </div>
  );
}

// Render App
ReactDOM.render(

  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
